const BaseElement = require('./BaseElement');

class Row extends BaseElement {
  constructor(rootElement) {
    super(rootElement);
    this.FirstName = this.rootElement.$('td:nth-of-type(1)');
    this.deleteBtn = this.rootElement.$('button[ng-click="delUser()"]')
  }

  getFirstName() {
    return this.FirstName.getText().then(text => console.log(text))
  }

  deleteRow() {
    return this.deleteBtn.click()
  }

}

module.exports = Row;
