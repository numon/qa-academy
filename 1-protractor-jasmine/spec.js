const CreateUser = require('./pages/CreateUser');
const createUser = new CreateUser();


describe('Protractor Demo App', function() {

  it('should have a title', function() {
    createUser.get('http://www.way2automation.com/angularjs-protractor/webtables/');
    createUser.createUser();
  });

  it('check that user created', function () {
    expect($$('.smart-table-data-row').count()).toEqual(8);
  });
});

