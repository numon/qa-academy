const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const { Given, Then } = require('cucumber');
const HomePage = require('../pageObjects/HomePage');

const homePage = new HomePage();

chai.use(chaiAsPromised);
const { expect } = chai;

Given(/^I open home page$/, () => homePage.open());
Then(/^I can verify "([^"]*)" url$/, url => expect(homePage.getUrl()).eventually.equal(url));
