const BasePage = require('./BasePage');

class CreateUser extends BasePage {

  constructor() {
    super();
    this.createUserBtn = $('button[ng-click="pop()"]');
    this.firstNameInput = $('input[name="FirstName"]');
    this.usersList = $$('.smart-table-data-row');
  }

  createUser() {
    this.createUserBtn.click();
    this.firstNameInput.sendKeys('testUser');
    $('input[name="LastName"]').sendKeys('testLastName');
    $('input[name="UserName"]').sendKeys('testUserName');
    $('input[name="Password"]').sendKeys('testpass');
    $('input[name="optionsRadios"][value="15"]').click();
    $('select[name="RoleId"]').sendKeys('Customer');
    $('input[name="Email"]').sendKeys('q@qqq.com');
    $('input[name="Mobilephone"]').sendKeys('33321312312');
    $('.btn-success').click();
    this.waitForElement(element(by.cssContainingText('td.smart-table-data-cell:nth-of-type(1)', 'testUser')))
    //browser.sleep(2000);
  }

  getTextBtn() {
    this.createUserBtn.getText().then(text => console.log(text));
  }

  deleteUser(userName) {
    return this.usersList.filter(userItem => {
      return userItem.$('.smart-table-data-cell:nth-of-type(1)').getText()
        .then(text => text === userName)
    }).then(usersList => {
      return usersList[0].$('button[ng-click="delUser()"]').click()
    })
    .then(() => $('.btn-primary').click())
  }

  usersListfn() {
    return this.usersList.map(userItem => {
      return {
        'First Name' : userItem.$('.smart-table-data-cell:nth-of-type(1)').getText(),
        'Last Name' : userItem.$('.smart-table-data-cell:nth-of-type(2)').getText()
      }
    })
  }

}

module.exports = CreateUser;
